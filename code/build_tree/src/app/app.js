'use strict';

var FileTreeNode = function(nodeId, name, type) {
  var children = [];

  this.nodeId = nodeId;
  this.name = name;
  this.type = type;
  this.parentNode = null;

  this.setParent = function(parentNode) {
    this.parentNode = parentNode;
  };
  this.addChild = function(node){
    if (this.type !== 'DIRECTORY') {
      throw "Cannot add child node to a non-directory node";
    }
    children.push(node);
    node.setParent(this);
  };
  this.getChildren = function() {
    return children;
  };
};

var FileTree = function() {
  this.nodes = [];

  this.getRoots = function (node) {
    var data = [];
    for(var i = 0; i< node.length; i++){
      data.push(node[i])
    }
    return data;
  }

  this.getChildren = function (node, root) {
    return node.filter(function(node){
      return node.parentId == root.id
    })
  } 

  this.createChildren = function (fileTree, roots, nodes, parent) {
    for (var root in roots) {
      fileTree.createNode(roots[root].id, roots[root].name, roots[root].type, parent);
      var children = fileTree.getChildren(nodes, roots[root]);
      var node = fileTree.findNodeById(roots[root].id);
      fileTree.createChildren(fileTree, children, nodes, node)
    }
  };

  this.getRootNodes = function () {
    var data = [];
    for (var i = 0; i < this.nodes.length; i++) {
      data.push(this.nodes[i]);
    }
    return data;
  };

  this.findNodeById = function(nodeId) {
    for (var i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].nodeId === nodeId) {
        return this.nodes[i];
      }
    }
    return null;
  };
  this.createNode = function(nodeId, name, type, parentNode) {
    var node = new FileTreeNode(nodeId, name, type);
    if (parentNode) {
      parentNode.addChild(node);
    }
    this.nodes.push(node);
  }
};

function createFileTree(input) {
  var fileTree = new FileTree();
  var rootNode = fileTree.getRoots(input);
  fileTree.createChildren(fileTree, rootNode, input);
  return fileTree;
}
