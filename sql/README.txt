Database structure
You are given one table:

```
CREATE TABLE customers (
  id INT NOT NULL PRIMARY KEY,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL
);
```
Problem statement
Find all the customers whose first and last names are not unique in our database, as well as the number of times each
first and last name pair appears in our database.
Sort your result by the total number of occurrences in descending order.